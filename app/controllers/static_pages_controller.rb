class StaticPagesController < ApplicationController
  def home
  end

  def photos
  end

  def raceInfo
  end

  def help
  end

  def about
  end

  def success
  end

  def contact
  end
end
